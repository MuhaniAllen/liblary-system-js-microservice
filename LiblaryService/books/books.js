const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

app.use(bodyParser.json());

require('./Book'); // import the book model file
const Book = mongoose.model('Book'); // load the model into the conatant(the name of the model as in the model file)
mongoose.connect('mongodb+srv://allen:Muhani123@testingcluster-yjajr.gcp.mongodb.net/test?retryWrites=true', () => {
    console.info("Database connected");
});


app.listen(3000, () => {
    console.log('App listening on port 3000!');
});

app.get('/', (req, res) => {
    res.send("This is my main route");
});


//  ======================== books route (Retrieving from db) ================= (start)

app.get('/books', (req, res) => {

    Book.find().then((books) => {
        res.json(books)
    }).catch(() => {
        if (erro) { throw erro }
    }); // returns for us all the books from our cloud databse 
});




app.get('/book/:id', (req, res) => {
    Book.findById(req.params.id).then((book) => {

        if (books) {
            res.json("null");
            console.log("null");
        } else {
            res.send("Book not found/ invalid id");
            console.log("Book not found/ invalid id");
        }

    }).catch((error) => {
        if (error) { throw error; }
    });
});
// ========================= books route (Retrieving from db) ================= (stop)




//  ========================book route (adding to db) =================== (start)
app.post('/book', (req, res) => {
    var newBook = {
        tittle: req.body.tittle,
        author: req.body.author,
        numberOfPages: req.body.numberOfPages,
        publisher: req.body.publisher
    }

    var book = new Book(newBook);
    book.save().then(() => {
        var response = "new book " + req.body.tittle + " created in the database";
        res.send(response);

    }).catch((err) => {
        if (err) {
            throw err;
        }

    });



});
//  ========================book route (adding to db) =================== (end)


//  ========================book delete (delete from db) =================== (start)


app.delete('/book/:id', (req, res) => {
    Book.findByIdAndRemove(req.params.id).then(() => {
        res.send("Book with ID : " + req.params.id + " has been deleted");
    }).catch((error) => {
        throw (error);
    });

});
//  ========================book delete (delete from db) =================== (end)