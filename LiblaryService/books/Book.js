// import * as mongoose from 'mongoose';
const mongoose = require('mongoose');
// creating a model for books
mongoose.model("Book", {
    // tittle, author, numberPages, publisher --- Attributes ill be working with in this model
    tittle: {
        type: String,
        require: true
    },
    author: {
        type: String,
        require: true
    },
    numberPages: {
        type: Number,
        require: false
    },
    publisher: {
        type: String,
        require: false
    }
});