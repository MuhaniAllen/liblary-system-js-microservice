const express = require('express'); //importing the express liblary
const app = express(); // calling the express liblary for use as const app
const bodyParser = require('body-parser'); //  liblary to send and recieve requests
app.use(bodyParser.json()); // specify the request type as json
const axios = require("axios");


//  connection to mongo database ======(Start)
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://allen:Muhani123@testingcluster-yjajr.gcp.mongodb.net/test?retryWrites=true', () => {
    console.info("Database connected");
});
//  connection to mongo database ======(End)


// connection to model =========(Start)
require('./Order');
const Order = mongoose.model('Order');
// connection to model =========(End)


// starting the service on port 3002 ====(Start) 
app.listen(3002, () => {
    console.log('App listening on port 3002!');
});
// starting the service on port 3002 ====(End) 



// ====== route to creat an order in the mongo database   ====(Start)
app.post('/order', (req, res) => {
    var newOrder = {
            CustomerID: mongoose.Types.ObjectId(req.body.CustomerID),
            BookID: mongoose.Types.ObjectId(req.body.BookID),
            initialDate: req.body.initialDate,
            deliveryDate: req.body.deliveryDate

        } // get data from request and store in json object
    var order = new Order(newOrder); // push the data to the model
    order.save().then(() => { // make a promise to save the data
        res.send("Order has been made successfully"); // report the the promise has been kept
    }).catch((err) => {
        if (err) { throw err; } // report that the promise has not been kept and why
    })
});
// ====== route to creat an order in the mongo database   ====(End)





// ======route to list orders by their id`s      ======= (Start)
app.get('/order/:id', (req, res) => {
    Order.findById(req.params.id).then((order) => {
        if (Order) {
            //    requires a liblary called axios to send http request to other services/servers 
            // installation === npm install --save axios
            axios.get("http//localhost:3001/customer/" + order.CustomerID).then((response) => {
                console.log(response);
            }).catch((err) => {
                if (err) { throw err; }
            })


        } else {
            res.send("Invalid order");
        }
    })
});
// ======route to list orders by their id`s      ======= (End)